package models;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import play.db.jpa.GenericModel;

@Entity
public class Tweet extends GenericModel
{
  @Id
  public String   id;
  public String   message_text;
  public String   contact;
  public String     date;

  @ManyToOne
  public Tweeter tweeter;
  
  public Tweet(Tweeter tweeter, String id, String message_text, String contact, String date)
  {
	this.tweeter = tweeter;
    this.id = id;
    this.message_text = message_text;
    this.contact = contact;
    this.date = date;
    		//new Date().getTime();
  }
} 
