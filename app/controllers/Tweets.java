package controllers;
import play.*;
import play.mvc.*;

import java.text.DateFormat;
import java.util.*;

import models.*;

public class Tweets extends Controller
{
  public static void index()
  {
    Tweeter tweeter = Accounts.getCurrentUser();
    
    Logger.info("Tweeter ctrler : user is " + tweeter.email);
    
    List<Tweet> tweets = tweeter.tweets;
    render(tweeter, tweets);
  }

  public static void writetweet(String message_text, String contact)
  {
    Tweeter tweeter = Accounts.getCurrentUser();
    String date = setDate();
    String id = UUID.randomUUID().toString();
    Tweet tweet = new Tweet(tweeter, id, message_text, contact, date);
    tweeter.tweets.add(tweet);
    Logger.info("tweet text " + message_text + " " + "conatct " + contact);
    tweeter.save();
    index();
  }

  
  private static String setDate()
  {
    DateFormat df = DateFormat.getDateTimeInstance();
    df.setTimeZone(TimeZone.getTimeZone("UTC"));
    String formattedDate = df.format(new Date());
    return formattedDate;
  }

  public static void deleteTweet(String id) {
		Tweet tweet = Tweet.findById(id);
		if (tweet == null) {
			notFound();
		} else {
			tweet.delete();
			//renderText("success");
			index();
		}
	}
  
  public static void showTweet(String id)
  {
	  Tweet tweet = Tweet.findById(id);
		if (tweet == null) {
			notFound();
		} else {
			
			//renderText("success");
			Tweets.special();
		}
	
  }
  
  /*public static void drop(Long id)
  {
	  String userId = session.get("logged_in_userid");
	    User user = User.findById(Long.parseLong(userId));
	    User friend = User.findById(id);
	    user.unfriend(friend);
	    Logger.info("Dropping " + friend.email);
	    index();
  }*/
  
  
  public static void renderlist()
  {
	Tweeter tweeter = Accounts.getCurrentUser();
	List<Tweet> tweets  = tweeter.tweets;
    render(tweets);
  }
  
  public static void special()
  {
	  Tweeter tweeter = Accounts.getCurrentUser();
	  List<Tweet> tweets  = tweeter.tweets;
	  render(tweeter, tweets); 
  }
}
